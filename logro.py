from controllers.image import *
from controllers.gb import *
from controllers.mp import *


app = webapp2.WSGIApplication([
    ('/', MainPage),
    ('/img', Image),
    ('/sign', Logro)
], debug=True)
