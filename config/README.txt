This folder contains a python file with a few configuration options,
including the URL of the web-page to be fetched, and the title of the application.
This is independent from the yaml config files, which are used by App Engine