from os import path
import urllib
import logging

import jinja2

import webapp2
from google.appengine.api import users

from __init__ import *

from models.models import *


JINJA_ENVIRONMENT = jinja2.Environment(loader=jinja2.FileSystemLoader(
    path.dirname('.')), extensions=['jinja2.ext.autoescape'], autoescape=True)


class MainPage(webapp2.RequestHandler):

    def get(self):
        logging.debug('MainPage::get()')
        logro_name = self.request.get(
            'logro_name', DEFAULT_LOGRO_NAME)
        greetings_query = Greeting.query(
            ancestor=logro_key(logro_name)).order(-Greeting.date)
        greetings = greetings_query.fetch(10)

        user = users.get_current_user()
        if user:
            url = users.create_logout_url(self.request.uri)
            url_linktext = 'Logout'
        else:
            url = users.create_login_url(self.request.uri)
            url_linktext = 'Login'

        template_values = {
            'user': user,
            'greetings': greetings,
            'logro_name': urllib.quote_plus(logro_name),
            'url': url,
            'url_linktext': url_linktext,
        }

        template = JINJA_ENVIRONMENT.get_template('views/index.html')
        self.response.write(template.render(template_values))
