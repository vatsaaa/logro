import urllib

import webapp2
from google.appengine.api import users
from google.appengine.api import app_identity

from __init__ import *
from models.models import *


class Logro(webapp2.RequestHandler):

    def post(self):
        logro_name = self.request.get(
            'logro_name', DEFAULT_LOGRO_NAME)

        greeting = Greeting(parent=logro_key(logro_name))

        userId = 'annonymous'
        if users.get_current_user():
            userId = users.get_current_user().user_id()
            greeting.author = Author(
                identity=userId, email=users.get_current_user().email())

        bucket_name = app_identity.get_default_gcs_bucket_name()
        greeting.content = self.request.get('content')
        greeting.avatar = self.request.get('img')
        file_name = getattr(self.request.POST.get('img'), 'filename')

        greeting.put()

        self.redirect(
            '/?' + urllib.urlencode({'logro_name': logro_name}))

        query_params = {'logro_name': logro_name}


